/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../../SerialPort.h"

class CircularTextBuffer
{
public:
    CircularTextBuffer() : startPosition (0), endPosition (0)
    {
        buffer.setSize (10);
    }

    void add (char c)
    {
        char* bufp = (char*)buffer.getData();
        bufp[endPosition] = c;

        if (++endPosition >= buffer.getSize())
            endPosition = 0;

        if (endPosition == startPosition)
        {
            if (++startPosition >= buffer.getSize())
                startPosition = 0;
        }
    }

    String toString()
    {
        String s;
        char*const bufp = (char* const)buffer.getData();
        s.preallocateBytes (buffer.getSize());

        if (startPosition <= endPosition)
        {
            s.appendCharPointer ((CharPointer_UTF8)&bufp[startPosition], (CharPointer_UTF8)&bufp[endPosition]);
        }
        else
        {
            s.appendCharPointer ((CharPointer_UTF8)&bufp[startPosition], (CharPointer_UTF8)&bufp[buffer.getSize()]);
            s.appendCharPointer ((CharPointer_UTF8)&bufp[0], (CharPointer_UTF8)&bufp[endPosition]);
        }

        return s;
    }
    
    bool isFull()
    {
        int diff = (int)endPosition - (int)startPosition;
        return diff == -1 || diff == (buffer.getSize() - 1);
    }
    
    void setSize (size_t newSize)
    {
        String s (toString());
        int diff = buffer.getSize() - newSize;
        int offset = diff < 0 ? 0 : diff;
        buffer.setSize (newSize);
        buffer.copyFrom (s.toRawUTF8() + offset, 0, s.length() - offset);
        startPosition = 0;
        endPosition = s.length() - offset;
    }
    
    size_t getSize()
    {
        return buffer.getSize();
    }
    
    void clear()
    {
        startPosition = 0;
        endPosition = 0;
    }

private:
    MemoryBlock buffer;
    size_t startPosition;
    size_t endPosition;
};

//==============================================================================
/* This component lives inside our window, and this is where you should put all
    your controls and content. */
class MainContentComponent :    public  Component,
                                private SerialPort::Listener,
                                private Button::Listener,
                                private Timer,
                                private TextEditor::Listener
{
public:
    //==============================================================================
    
    MainContentComponent();
    ~MainContentComponent();

    //Component
    void paint (Graphics&) override;
    void resized() override;
    
    //SerialPort::Listener
    //void serialDataRecieved (const int portIndex, const uint8 byte)
    void serialDataRecieved (const String& /*portName*/, const uint8 byte) override;
    void serialConnectionError(const unsigned int errorCode) override {};
    // Called when the connection timesout
    void serialPortTimeout() override {};

    
    //Button::Listener
    void buttonClicked (Button* button) override;

    //Timer
    void timerCallback() override;
    
    //Label::Listener
    void textEditorReturnKeyPressed (TextEditor&)  override;

private:
    SerialPort sp;
    TextButton connectButton, clearButton, sendButton;
    ComboBox deviceCombobox;
    TextEditor receiveText, sendText;
    
    Atomic<int> serialDataFlag;
    CriticalSection textBufferLock;
    CircularTextBuffer textBuffer;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
