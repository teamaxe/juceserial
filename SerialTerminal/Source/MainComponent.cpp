	/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainContentComponent::MainContentComponent()
{
    setSize (500, 400);
    
    connectButton.setButtonText ("Connect");
    connectButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    connectButton.addListener (this);
    addAndMakeVisible (&connectButton);
    
    clearButton.setButtonText ("Clear");
    clearButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    clearButton.addListener (this);
    addAndMakeVisible (&clearButton);

    sendButton.setButtonText ("Send");
    sendButton.setConnectedEdges (Button::ConnectedOnLeft | Button::ConnectedOnRight);
    sendButton.addListener (this);
    addAndMakeVisible (&sendButton);
    
    StringArray ports;
    SerialPort::getDevicePathList (ports);
    if (ports.size())
    {
        deviceCombobox.addItemList (ports, 1);
        deviceCombobox.setSelectedId (1);
    }
    addAndMakeVisible (deviceCombobox);
    
    receiveText.setMultiLine (true);
    receiveText.setReadOnly (true);
    receiveText.setScrollbarsShown (false);
    addAndMakeVisible (receiveText);

    sendText.setTextToShowWhenEmpty ("Insert text here...", Colours::grey);
    sendText.addListener (this);
    addAndMakeVisible (sendText);
    
    
    sp.addListener (this);

    startTimer (100); //10 times a second
}

MainContentComponent::~MainContentComponent()
{
    sp.removeListener (this);
    
    connectButton.removeListener (this);
}

void MainContentComponent::paint (Graphics& g)
{
    g.fillAll (Colour (0xffeeddff));

    g.setFont (Font (16.0f));
    g.setColour (Colours::black);
    g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
    juce::Rectangle<int> r (getLocalBounds());
    juce::Rectangle<int> top (r.removeFromTop (20));
    deviceCombobox.setBounds (top.removeFromLeft ((int)(r.getWidth() * 0.75)));
    connectButton.setBounds (top.removeFromLeft (top.getWidth() / 2));
    clearButton.setBounds (top);
    
    juce::Rectangle<int> bottom (r.removeFromBottom (20));
    sendButton.setBounds (bottom.removeFromRight (clearButton.getWidth()));
    sendText.setBounds (bottom);

    receiveText.setBounds (r);
}

void MainContentComponent::serialDataRecieved (const String& /*portName*/, const uint8 byte)
{
    {
        ScopedLock sl (textBufferLock);
        textBuffer.add (byte);
    }
    serialDataFlag = true;
}

void MainContentComponent::buttonClicked (Button* button)
{
    if (button == &connectButton)
    {
        if (connectButton.getToggleState())
        {
            //sp.closeAllPorts();
            sp.closePort (deviceCombobox.getItemText (deviceCombobox.getSelectedId() - 1));
            connectButton.setToggleState (false, dontSendNotification);
        }
        else
        {
            SerialPort::SerialPortSettings sps (deviceCombobox.getItemText (deviceCombobox.getSelectedId() - 1), SerialPort::Baud9600);
            sp.openPort (sps);
            connectButton.setToggleState (true, dontSendNotification);
        }
    }
    else if (button == &sendButton)
    {
        sp.writeToPort (deviceCombobox.getItemText (deviceCombobox.getSelectedId() - 1), sendText.getText().toUTF8(), sendText.getText().length()); 
    }
    else if (button == &clearButton)
    {
        ScopedLock sl (textBufferLock);
        textBuffer.clear();
        serialDataFlag = true;
    }
}

void MainContentComponent::timerCallback()
{
    if (serialDataFlag.compareAndSetBool (false, true))//only draw when necessary
    {
        String s;
        {
            ScopedLock sl (textBufferLock);
            s = textBuffer.toString();
        }
        receiveText.setText (s);
        //if the text in the textBuffer over/underfills the textBox then adjust the size of the textBuffer
        int lineHeight = receiveText.getFont().getHeight();
        int textLines = receiveText.getTextHeight() / lineHeight;
        int boxLines = receiveText.getHeight() / lineHeight;
        //textlines must be smaller than boxLines to prevent scrolling
        int boxLinseMinusTextLines = (boxLines - 1) - textLines;
        DBG ("boxHeight:" << boxLines << "textHeight:" << textLines << "boxHeightMinusTextHeight" << boxLinseMinusTextLines << "lineHeight");
        if (boxLinseMinusTextLines < 0)//the text is taller than textBox
        {
            DBG ("=======Reduce Text===========");
            DBG ("Before:" << (int)textBuffer.getSize() << ":" << textBuffer.toString());
            DBG ("OldSize:" << (int)textBuffer.getSize() << ": NewSize:" << (int)(textBuffer.getSize() - receiveText.getTextIndexAt (0, 0)));
            textBuffer.setSize (textBuffer.getSize() - receiveText.getTextIndexAt (0, 0));
            DBG ("After:"  << (int)textBuffer.getSize() << ":" << textBuffer.toString());
        }
        else if (textBuffer.isFull() && boxLinseMinusTextLines > 0)
        {
            DBG ("=======Enlarge Text===========");
            DBG ("Before:" << (int)textBuffer.getSize() << ":" << textBuffer.toString());
            float sf = 1.f / ((float)textLines / boxLines);
            DBG ("NewSize:" << textBuffer.getSize() * sf);
            textBuffer.setSize (textBuffer.getSize() * sf);
            DBG ("After:" << (int)textBuffer.getSize() << ":" << textBuffer.toString());
            //enlarge text
            //textBuffer.setSize
        }
    }
}

void MainContentComponent::textEditorReturnKeyPressed (TextEditor&)
{
    buttonClicked (&sendButton);
}
